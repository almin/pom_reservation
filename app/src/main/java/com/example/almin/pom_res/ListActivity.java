package com.example.almin.pom_res;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

public class ListActivity extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        // Test restaurant listing
        //String[] items = new String[] { "Vegetables","Fruits","Flower Buds","Legumes","Bulbs","Tubers" };

        //restaurantsToList(items);
    }

    public void restaurantsToList(String[] restaurantsList) {

        ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, restaurantsList);

        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(listAdapter);
    }
}
